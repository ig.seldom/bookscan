import { Paper, Typography } from '@mui/material'
import React from 'react'

const Empty = () => {
    return (
        <Paper elevation={0}>
            <Typography align='center' variant='body1'>List is empty</Typography>
        </Paper>
    )
}

export default Empty