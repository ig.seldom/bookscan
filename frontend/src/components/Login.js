import React, { useState } from 'react'
import { apiURL } from '../api'
import { createCookie } from '../utils'
import withGlobalStateProps from "../context/with-global-state-props";
import { Alert, Box, Button, Checkbox, FormControlLabel, TextField } from '@mui/material';
import { Stack } from '@mui/system';

const Login = ({ setIsLoggedIn, setTitle, history }) => {
  const [state, setState] = useState({
    email: '',
    password: '',
    isSubmitting: false,
    message: '',
  })

  const { message } = state

  const handleChange = async e => {
    const { name, value } = e.target
    await setState({ ...state, [name]: value })
  }

  const handleSubmit = async () => {
    setState({ ...state, isSubmitting: true })
    setTitle("Login")
    const { email, password, remember } = state
    fetch(`${apiURL}/login`, {
      method: 'POST',
      body: JSON.stringify({
        email,
        password,
        remember,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    }).then(res => {
      res.json().then(r => {
        const { token, success, msg } = r
        if (success) {
          createCookie('token', token, 2147483647)
          setIsLoggedIn(true)
          history.push({ pathname: '/market', state: null })
        } else {
          setState({
            ...state,
            message: msg,
            isSubmitting: false,
          })
        }
      }).catch(e => {
        setState({ ...state, message: e.toString(), isSubmitting: false })
      })
    }).catch(e => {
      setState({ ...state, message: e.toString(), isSubmitting: false })
    })
  }

  return (
    <Box
      component="form"
      sx={{ p: 2, mt: 2, mb: 2, border: '1px dashed grey' }}
      noValidate
      autoComplete="off"
    >
      <Stack spacing={2}>
        {(message.length > 0) ? (<Alert severity="error">{message}</Alert>) : ""}
        <TextField id="email" type="email" name="email" label="E-mail" variant="outlined" onChange={e => { handleChange(e) }} />
        <TextField id="password" type="password" name="password" label="Password" variant="outlined" onChange={e => { handleChange(e) }} />
        <FormControlLabel control={<Checkbox defaultChecked={false} value="yes" name="remember" onChange={e => { handleChange(e) }} />} label="Remember me" />
        <Button variant="contained" size="large" onClick={() => handleSubmit()}>
          Login
        </Button>
      </Stack>
    </Box >
  )
}

export default withGlobalStateProps(Login)
