import React, { } from "react"
import { tz } from "../utils/timezone"
import { formatPrice } from "../utils/number"
import { Button, Paper, Table, TableBody, TableCell, TableContainer, TableRow, Typography } from "@mui/material"

const MarketItem = ({ props, openDetailModal }) => {
    const { home, away, league, startsAt, homePrice, drawPrice, awayPrice } = props
    const firstLastPrice = (price) => {
        let first = price[0]
        let last = price[price.length - 1]
        return [first, last]
    }

    const Prices = ({ values }) => {
        if (values.length === 1) {
            const last = values[0]
            return (
                <TableCell align="right">
                    <Typography variant="body1" component="span">
                        {formatPrice(last)}
                    </Typography>
                </TableCell>
            )
        }
        if (values.length > 1) {
            const [first, last] = firstLastPrice(values)
            const diff = last - first
            const diffSign = (diff >= 0) ? "+" : ""
            return (
                <TableCell align="right">
                    <Typography variant="body1" component="span">
                        {formatPrice(first)} ... {formatPrice(last)} ({diffSign}{formatPrice(diff)})
                    </Typography>
                </TableCell>
            )
        }
        return (
            <TableCell align="left">Empty</TableCell>
        )
    }

    return (
        <TableContainer component={Paper}>
            <Table size="small" aria-label="a dense table">
                <TableBody>
                    <TableRow>
                        <TableCell>{league}</TableCell>
                        <TableCell align="right">{tz(startsAt)}</TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell>{home}</TableCell>
                        <Prices values={homePrice} />
                    </TableRow>
                    <TableRow>
                        <TableCell>Draw</TableCell>
                        <Prices values={drawPrice} />
                    </TableRow>
                    <TableRow>
                        <TableCell>{away}</TableCell>
                        <Prices values={awayPrice} />
                    </TableRow>
                    <TableRow>
                        <TableCell />
                        <TableCell align="right">
                            <Button onClick={() => { openDetailModal(props) }}>Detail</Button>
                        </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default MarketItem