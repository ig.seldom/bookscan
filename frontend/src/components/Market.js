import React, { useState, useEffect } from 'react'
import { apiURL } from '../api'
import MarketItem from "./MarketItem";
import Empty from "./Empty";
import withGlobalStateProps from '../context/with-global-state-props';
import { Box, Stack } from '@mui/system';
import { Alert, LinearProgress } from '@mui/material';
import FullScreenDialog from './FullScreenDialog';

const Market = ({ setIsLoggedIn, setTitle, history }) => {
    const [isLoading, setIsLoading] = useState(false)
    const [message, setMessage] = useState("")
    const [market, setMarket] = useState(null)
    const [isDetailOpened, setIsDetailOpened] = useState(false)
    const [currentMarketItem, setCurrentMarketItem] = useState(null)
    const openDetailModal = (item) => {
        setIsDetailOpened(true)
        setCurrentMarketItem(item)
    }
    const getMarket = () => {
        setTitle("Football")
        setIsLoading(true)
        fetch(`${apiURL}/market`, {
            method: 'GET',
            credentials: 'include',
            headers: {
                Accept: 'application/json',
                Authorization: document.cookie,
            },
        }).then(res => {
            if (res.status === 401) {
                setIsLoggedIn(false)
                history.push("/login")
            }
            res.json().then(r => {
                setIsLoading(false)
                setMarket(r.market)
            })
        }).catch(e => {
            setIsLoading(false)
            setMessage(e.toString())
        })
    }

    useEffect(() => {
        getMarket()
        return () => {
        }
    }, [])

    return (
        <React.Fragment>
            {message && (<Alert severity="error">{message}</Alert>)}
            {isLoading && (
                <Box sx={{ width: '100%' }}>
                    <LinearProgress />
                </Box>
            )}
            <Stack spacing={2}>
                {market && market.length > 0 && market.map(el => {
                    let key = `${el.home} - ${el.away} / ${el.league} @ ${el.startsAt}`
                    return <MarketItem key={key} props={el} openDetailModal={openDetailModal} />
                })}
            </Stack>
            {market && market.length === 0 && <Empty />}
            {isDetailOpened && (
                <FullScreenDialog isDetailOpened={isDetailOpened} setIsDetailOpened={setIsDetailOpened} currentMarketItem={currentMarketItem} />
            )}
        </React.Fragment>
    )
}

export default withGlobalStateProps(Market)
