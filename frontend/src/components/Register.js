import { Alert, Box, Button, Checkbox, FormControlLabel, Stack, TextField } from '@mui/material'
import React, { useState } from 'react'
import { apiURL } from '../api'
import withGlobalStateProps from '../context/with-global-state-props'

const Register = ({ history, setTitle, setIsLoggedIn }) => {
  const [state, setState] = useState({
    email: '',
    password: '',
    passwordRepeat: '',
    termsAndConditions: '',
    isSubmitting: false,
    message: '',
    errors: null,
  })

  setTitle("Register")

  const { message, isSubmitting, errors } = state

  const handleChange = async e => {
    await setState({ ...state, [e.target.name]: e.target.value })
  }

  const handleSubmit = async e => {
    setState({ ...state, isSubmitting: true })

    const { email, password, termsAndConditions, passwordRepeat } = state
    try {
      const res = await fetch(`${apiURL}/register`, {
        method: 'POST',
        body: JSON.stringify({
          email,
          password,
          passwordRepeat,
          termsAndConditions,
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      }).then(res => res.json())
      const { success, msg, errors } = res

      if (!success) {
        return setState({ ...state, message: msg, errors, isSubmitting: false })
      }
      setIsLoggedIn(false)
      history.push('/login')
    } catch (e) {
      setState({ ...state, message: e.toString(), isSubmitting: false })
    }
  }
  return (
    <Box
      component="form"
      sx={{ p: 2, mt: 2, mb: 2, border: '1px dashed grey' }}
      noValidate
      autoComplete="off"
    >
      <Stack spacing={2}>
        {message && (<Alert severity="error">{message}</Alert>)}
        {errors && errors.length > 0 && errors.map((error, id) => (<Alert severity="error" key={id}>{error}</Alert>))}
        <TextField id="email" type="email" name="email" label="E-mail" variant="outlined" onChange={e => { handleChange(e) }} />
        <TextField id="password" type="password" name="password" label="Password" variant="outlined" onChange={e => { handleChange(e) }} />
        <TextField id="passwordRepeat" type="password" name="passwordRepeat" label="Repeat Password" variant="outlined" onChange={e => { handleChange(e) }} />
        <FormControlLabel control={<Checkbox defaultChecked={false} value="yes" name="termsAndConditions" onChange={e => { handleChange(e) }} />} label="I agree with terms and conditions" />
        <Button disabled={isSubmitting} variant="contained" size="large" onClick={() => handleSubmit()}>
          Sign Up
        </Button>
      </Stack>
    </Box >
  )
}

export default withGlobalStateProps(Register)
