import { AppBar, Dialog, IconButton, Paper, Slide, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Toolbar, Typography } from "@mui/material";
import React, { forwardRef } from "react";
import CloseIcon from '@mui/icons-material/Close';
import { formatPrice } from "../utils/number"

const Transition = forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function FullScreenDialog({ isDetailOpened, setIsDetailOpened, currentMarketItem }) {
    const handleClose = () => {
        setIsDetailOpened(false);
    };

    const Prices = ({ home, away, draw }) => {
        const merge = (home, away, draw) => {
            let prices = []
            for (let i = 0; i < home.length; i++) {
                prices[i] = { home: home[i], away: away[i], draw: draw[i] }
            }
            return prices
        }

        return merge(home, away, draw).map((item, idx) => (
            <TableRow key={idx}>
                <TableCell align="right">{formatPrice(item.home)}</TableCell>
                <TableCell align="center">{formatPrice(item.draw)}</TableCell>
                <TableCell align="left">{formatPrice(item.away)}</TableCell>
            </TableRow>
        ))
    }

    return (
        <div>
            <Dialog
                fullScreen
                open={isDetailOpened}
                onClose={handleClose}
                TransitionComponent={Transition}
            >
                <AppBar sx={{ position: 'relative' }}>
                    <Toolbar>
                        <IconButton
                            edge="start"
                            color="inherit"
                            onClick={handleClose}
                            aria-label="close"
                        >
                            <CloseIcon />
                        </IconButton>
                        <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                            {currentMarketItem.league}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Paper elevation={0} sx={{ m: 3 }}>
                    <TableContainer component={Paper}>
                        <Table size="small" aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="right">{currentMarketItem.home}</TableCell>
                                    <TableCell align="center">Draw</TableCell>
                                    <TableCell align="left">{currentMarketItem.away}</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <Prices home={currentMarketItem.homePrice} away={currentMarketItem.awayPrice} draw={currentMarketItem.drawPrice} />
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Paper>
            </Dialog>
        </div>
    );
}