import React, { useState, useEffect } from 'react'
import { apiURL } from '../api'
import './EventList.css'
import EventItem from "./EventItem"
import Empty from "./Empty"
const EventList = ({ history }) => {
    const [state, setState] = useState({
        isFetching: false,
        message: "",
        eventList: [],
    })

    const { eventList, isFetching, message } = state

    const getEventList = () => {
        setState({ ...state, isFetching: true, message: 'fetching details...' })
        fetch(`${apiURL}/eventList`, {
            method: 'GET',
            credentials: 'include',
            headers: {
                Accept: 'application/json',
                Authorization: document.cookie,
            },
        }).then(res => {
            if (res.status === 401) {
                history.push("/login")
            }
            res.json().then(r => {
                setState({ ...state, isFetching: false, eventList: r.eventList })
            })
        }).catch(e => {
            setState({ ...state, message: e.toString(), isFetching: false })
        })
    }

    useEffect(() => {
        if (history.location.state) {
            return setState({ ...state, eventList: { ...history.location.state } })
        }
        getEventList()
        return () => {
        }
    }, [])

    return (
        <React.Fragment>
            {isFetching} <div>{message}</div>
            {eventList && eventList.length > 0 && eventList.map(el => {
                return <EventItem key={el.id} props={el} />
            })}
            {eventList && eventList.length === 0 && Empty}
        </React.Fragment>
    )
}

export default EventList
