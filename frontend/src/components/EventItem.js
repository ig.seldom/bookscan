import React from "react"
import {tz} from "../utils/timezone"
import {formatPrice} from "../utils/number"

const EventItem = ({props}) => {
    const {league, home, away, startsAt, prices} = props

    let renderPrice = (prices, type) => {
        return prices.map((price, i) => {
            let className = "badge badge-secondary text-black"
            let p = price[type]
            console.log(p[i + 1] && (2 <= p[i] && 2.5 >= p[i]) && p[i] > p[i + 1])
            if(p[i + 1] && (2 <= p[i] && 2.5 >= p[i]) && p[i] > p[i + 1]){
                className = "badge badge-success text-black"
            }
            return <span key={`${type}-${i}`} className={className}>{formatPrice(p)}</span>
        })
    }

    return (
        <div className="card m-1">
            <div className="card-body">
                <h5 className="card-title">{league} @ {tz(startsAt)}</h5>
                <div className="row">
                    <div className="col-sm-2">{home}</div>
                    <div className="col-sm-10">{renderPrice(prices, "home")}</div>
                </div>
                <div className="row">
                    <div className="col-sm-2">Draw</div>
                    <div className="col-sm-10">
                        {renderPrice(prices, "draw")}
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-2">{away}</div>
                    <div className="col-sm-10">
                        {renderPrice(prices, "away")}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default EventItem