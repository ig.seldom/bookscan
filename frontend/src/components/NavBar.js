import React from 'react'
import { Link as RouterLink } from "react-router-dom"
import withGlobalStateProps from "../context/with-global-state-props"
import {
    AppBar,
    Box,
    Toolbar,
    Typography,
    Button,
} from "@mui/material"
import { deleteCookie } from '../utils'

const NavBar = ({ isLoggedIn, setIsLoggedIn, title }) => {
    const menuItems = [
        { href: "/register", title: "Register", loggedIn: false },
        { href: "/login", title: "Login", loggedIn: false },
        { href: "/market", title: "Market", loggedIn: true },
        { href: "/live", title: "Live", loggedIn: true },
    ]

    const handleLogout = () => {
        deleteCookie('token')
        setIsLoggedIn(false)
    }

    return (
        <Box >
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        {title}
                    </Typography>
                    {menuItems.filter(el => el.loggedIn === isLoggedIn).map((el, i) => (
                        <Button key={i} component={RouterLink} to={el.href} color="inherit">
                            {el.title}
                        </Button>
                    ))}
                    {isLoggedIn && (
                        <Button component={RouterLink} to="/login" onClick={handleLogout} color="inherit">
                            Logout
                        </Button>
                    )}
                </Toolbar>
            </AppBar>
        </Box>
    )
}

export default withGlobalStateProps(NavBar)