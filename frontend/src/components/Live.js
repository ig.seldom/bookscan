import { Alert, Box, LinearProgress, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material'
import React, { useState, useEffect, Fragment } from 'react'
import { apiURL } from '../api'
import withGlobalStateProps from '../context/with-global-state-props'
import Empty from "./Empty"
const Live = ({ history, setIsLoggedIn, setTitle }) => {
    const [isLoading, setIsLoading] = useState(false)
    const [message, setMessage] = useState("")
    const [lives, setLives] = useState(null)

    const getLiveList = () => {
        setTitle("Live")
        setIsLoading(true)
        fetch(`${apiURL}/live`, {
            method: 'GET',
            credentials: 'include',
            headers: {
                Accept: 'application/json',
                Authorization: document.cookie,
            },
        }).then(res => {
            if (res.status === 401) {
                setIsLoggedIn(false)
                history.push("/login")
            }
            res.json().then(r => {
                setLives(r.live)
            })
            setIsLoading(false)
        }).catch(e => {
            setIsLoading(false)
            setMessage(e.toString())
        })
    }

    useEffect(() => {
        getLiveList()
        return () => {
        }
    }, [])

    const LiveTable = ({ lives }) => {
        return (
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
                    <TableHead>
                        <TableRow>
                            <TableCell>League</TableCell>
                            <TableCell>Home</TableCell>
                            <TableCell>Away</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {lives.map(el => {
                            let homeP = el.participants.filter(p => p.alignment === "home").shift()
                            let awayP = el.participants.filter(p => p.alignment === "away").shift()
                            return (
                                <TableRow key={el.id}>
                                    <TableCell component="th" scope="row">
                                        {el.league.name}
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        {homeP.name} ({homeP.state.score})
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        {awayP.name} ({awayP.state.score})
                                    </TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        )
    }

    return (
        <Fragment>
            {isLoading && (
                <Box sx={{ width: '100%' }}>
                    <LinearProgress />
                </Box>
            )}
            {message && (<Alert severity="error">{message}</Alert>)}
            {(lives && lives.length > 0) && <LiveTable lives={lives} />}
            {(lives && lives.length === 0) && <Empty />}
        </Fragment>
    )
}

export default withGlobalStateProps(Live)
