import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Register from "./components/Register";
import Login from "./components/Login";
import Live from "./components/Live"
import Market from "./components/Market"
import NavBar from "./components/NavBar"

function App() {
    return (
        <BrowserRouter>
            <NavBar />
            <Route exact path="/" component={Login} />
            <Route path="/register" component={Register} />
            <Route path="/login" component={Login} />
            <Route path="/market" component={Market} />
            <Route path="/live" component={Live} />
        </BrowserRouter>
    );
}

export default App;
