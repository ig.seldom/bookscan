let apiURL = "/api"

if (process.env.NODE_ENV !== 'production') {
    apiURL = "http://localhost:3001/api"
}

export { apiURL }