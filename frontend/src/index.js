import { Container, CssBaseline } from "@mui/material";
import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import App from "./App";
import GlobalStateContext from "./context/global-state";

const AppWrapper = () => {
    const [isLoggedIn, setIsLoggedIn] = useState(!!localStorage.getItem("logged_in"))
    const [title, setTitle] = useState("Application")

    useEffect(() => {
        localStorage.setItem("logged_in", isLoggedIn)
    }, [isLoggedIn])

    return (
        <GlobalStateContext.Provider value={{ isLoggedIn, setIsLoggedIn, title, setTitle }}>
            <CssBaseline />
            <Container disableGutters={true}>
                <App />
            </Container>
        </GlobalStateContext.Provider>
    )
}

ReactDOM.render(<AppWrapper />, document.getElementById("root"));
