import React from 'react'

const GlobalStateContext = React.createContext({
    isLoggedIn: false,
    setIsLoggedIn: () => { },
    title: "Application",
    setTitle: () => { },
})

export default GlobalStateContext