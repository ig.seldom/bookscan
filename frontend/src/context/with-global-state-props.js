import React from 'react'
import GlobalStateContext from "./global-state";

const withGlobalStateProps = (Component) => {
    return (props) => (
        <GlobalStateContext.Consumer>
            {({ isLoggedIn, setIsLoggedIn, title, setTitle }) => (
                <Component
                    isLoggedIn={isLoggedIn}
                    setIsLoggedIn={setIsLoggedIn}
                    title={title}
                    setTitle={setTitle}
                    {...props}
                />
            )}
        </GlobalStateContext.Consumer>
    )
}

export default withGlobalStateProps