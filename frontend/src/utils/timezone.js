import moment from 'moment-timezone'
import 'moment/locale/ru'

const appTimeZone = "Europe/Moscow"
moment.locale("en")

export let tz = (time) => {
    return moment.tz(time, appTimeZone).calendar()
}