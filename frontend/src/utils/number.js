export let formatPrice = (num) => {
    return Number(num).toFixed(2)
}