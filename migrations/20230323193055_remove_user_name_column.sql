-- +goose Up
-- +goose StatementBegin
alter table public.users drop column name;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
ALTER TABLE public.users ADD "name" varchar(100) NOT NULL default 'change me';
-- +goose StatementEnd
