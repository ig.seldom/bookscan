-- +goose Up
-- +goose StatementBegin
ALTER TABLE commands RENAME TO participants;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
ALTER TABLE participants RENAME TO commands;
-- +goose StatementEnd
