-- +goose Up
-- +goose StatementBegin
ALTER TABLE public.match_prices RENAME COLUMN home_price TO home;
ALTER TABLE public.match_prices RENAME COLUMN away_price TO away;
ALTER TABLE public.match_prices RENAME COLUMN draw_price TO draw;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
ALTER TABLE public.match_prices RENAME COLUMN home TO home_price;
ALTER TABLE public.match_prices RENAME COLUMN away TO away_price;
ALTER TABLE public.match_prices RENAME COLUMN draw TO draw_price;
-- +goose StatementEnd
