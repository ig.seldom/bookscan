-- +goose Up
-- +goose StatementBegin
create table if not exists match_notify
(
    id serial primary key,
    chat_id int not null,
    match_id int not null,
    created_at timestamp default CURRENT_TIMESTAMP not null
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table match_notify
-- +goose StatementEnd
