-- +goose Up
-- +goose StatementBegin
ALTER TABLE public.match_prices ADD created_at timestamp NULL;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
ALTER TABLE public.match_prices DROP COLUMN created_at;
-- +goose StatementEnd
