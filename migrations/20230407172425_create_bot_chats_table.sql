-- +goose Up
-- +goose StatementBegin
create table if not exists bot_chats
(
    id serial primary key,
    chat_id int not null,
    active boolean not null,
    created_at timestamp default CURRENT_TIMESTAMP not null,
    updated_at timestamp default CURRENT_TIMESTAMP not null
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table bot_chats
-- +goose StatementEnd
