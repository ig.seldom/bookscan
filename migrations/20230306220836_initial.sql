-- +goose Up
-- +goose StatementBegin
create table if not exists users
(
    id         serial
        primary key,
    name       varchar(100)                        not null,
    password   varchar(355)                        not null,
    email      varchar(355)                        not null
        unique,
    created_on timestamp default CURRENT_TIMESTAMP not null,
    updated_at timestamp default CURRENT_TIMESTAMP not null
);
create table if not exists leagues
(
    id         serial
        constraint leagues_pk
            primary key,
    name       varchar                 not null,
    created_at timestamp default now() not null,
    updated_at timestamp default now() not null
);

create table if not exists commands
(
    id         serial
        constraint commands_pk
            primary key,
    name       varchar not null,
    created_at timestamp default now(),
    update_at  timestamp default now()
);

create table if not exists match_table
(
    id         serial
        constraint match_table_pk
            primary key,
    home_id    integer                 not null,
    away_id    integer                 not null,
    league_id  integer                 not null,
    started_at timestamp               not null,
    created_at timestamp default now() not null
);

create table if not exists match_prices
(
    id         serial
        constraint match_prices_pk
            primary key,
    match_id   integer          not null,
    home_price double precision not null,
    away_price double precision not null,
    draw_price double precision not null,
    price_type varchar          not null
);

alter table match_table
    add constraint foreign_key_name
        foreign key (league_id) references leagues (id)
            on update cascade on delete cascade;

alter table match_table
    add constraint away_id_command_id_fk
        foreign key (away_id) references commands (id)
            on update cascade on delete cascade;

alter table match_table
    add constraint home_id_command_id_fk
        foreign key (home_id) references commands (id)
            on update cascade on delete cascade;

alter table match_table
    add constraint league_id_leagues_id
        foreign key (league_id) references leagues
            on update cascade on delete cascade;

alter table match_prices
    add constraint match_id_match_table_id_fk
        foreign key (match_id) references match_table (id)
            on update cascade on delete cascade;
create table if not exists match_score
(
    id serial,
    match_id integer not null,
    home integer not null,
    away integer not null
);

alter table match_score
    add constraint match_score_match_id_match_table_id
        foreign key (match_id) references match_table (id)
            on update cascade on delete cascade;

create index if not exists match_score_match_id on match_score (match_id);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table users
drop table leagues
drop table commands
drop table match_table
drop table match_prices
drop table match_prices
-- +goose StatementEnd
