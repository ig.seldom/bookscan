-- +goose Up
-- +goose StatementBegin
ALTER TABLE public.participants RENAME COLUMN update_at TO updated_at;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
ALTER TABLE public.participants RENAME COLUMN updated_at TO update_at;
-- +goose StatementEnd
