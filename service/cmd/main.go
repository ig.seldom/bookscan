package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/ig.seldom/bookscan/internal/server/controller"
	"gitlab.com/ig.seldom/bookscan/internal/server/notification"
	"gitlab.com/ig.seldom/bookscan/internal/server/router"
	"gitlab.com/ig.seldom/bookscan/internal/server/service"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	notify, err := notification.NewNotifier(os.Getenv("TG_BOT_KEY"))
	if err != nil {
		panic("can't create notification service")
	}
	app := service.NewService(ctx, notify)

	go func() {
		exit := make(chan os.Signal, 1)
		signal.Notify(exit,
			syscall.SIGHUP,
			syscall.SIGINT,
			syscall.SIGTERM,
			syscall.SIGQUIT,
		)
		<-exit
		cancel()
	}()

	app.ListenChatSubscribers()
	app.NotifySubscribers()
	if err := app.ConnectToDB(os.Getenv("CONNECTION_STRING")); err != nil {
		log.Fatal(err)
	}
	if err := app.RunScanner(); err != nil {
		log.Fatal(err)
	}
	controller.UseApp(app)

	r := router.SetupRouter()
	server := http.Server{
		Addr:    os.Getenv("SERVING_ADDRESS"),
		Handler: r,
	}

	go func() {
		<-ctx.Done()
		server.Shutdown(ctx)
		if err := app.DisconnectFromDB(); err != nil {
			log.Println("Error while disconnecting from db", err)
		}
	}()

	if err := server.ListenAndServe(); err != http.ErrServerClosed {
		log.Println(err)
	}
}
