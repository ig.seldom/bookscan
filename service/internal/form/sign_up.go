package form

type SignUp struct {
	Email                    string `json:"email"`
	Password                 string `json:"password"`
	PasswordRepeat           string `json:"passwordRepeat"`
	AcceptTermsAndConditions string `json:"termsAndConditions"`
}
