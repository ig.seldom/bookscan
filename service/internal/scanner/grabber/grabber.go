package grabber

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"

	"github.com/google/uuid"
	"gitlab.com/ig.seldom/bookscan/internal/scanner/nestedmap"
	"gitlab.com/ig.seldom/bookscan/internal/scanner/pinnacle"
)

type Grabber struct {
	config     AppConfig
	deviceUuid string
}

func New(config AppConfig) *Grabber {
	deviceUUID, _ := uuid.NewUUID()
	return &Grabber{
		config:     config,
		deviceUuid: deviceUUID.String(),
	}
}

func (g *Grabber) BuildRequest(path string, params []map[string]string) *http.Request {
	guestRoot, err := g.config.GetString("api", "haywire", "routes", "curacao", "guestRoot")
	if err != nil {
		log.Println(err)
	}
	location, err := url.Parse(guestRoot)
	if err != nil {
		log.Println(err)
	}
	q := location.Query()
	for _, set := range params {
		for k, v := range set {
			q.Set(k, v)
		}
	}

	apiVersion, err := g.config.GetString("api", "haywire", "apiVersion")
	if err != nil {
		log.Println(err)
	}
	location.Scheme = "https"
	location.Path = fmt.Sprintf("/%s/%s", apiVersion, path)
	location.RawQuery = q.Encode()

	r, err := http.NewRequest("GET", location.String(), nil)
	if err != nil {
		log.Println(err)
	}
	apiKey, err := g.config.GetString("api", "haywire", "apiKey")
	if err != nil {
		log.Println(err)
	}
	r.Header.Set("x-api-key", apiKey)
	return r
}

// Fetch fetches http resource
func Fetch[T any](r *http.Request) (T, error) {
	var result T
	client := http.Client{}
	res, err := client.Do(r)
	if err != nil {
		return result, err
	}
	defer func() {
		err = res.Body.Close()
		if err != nil {
			log.Println(err)
		}
	}()
	if res.StatusCode == http.StatusOK {
		body, err := io.ReadAll(res.Body)
		if err != nil {
			return result, err
		}
		err = json.Unmarshal(body, &result)
		return result, err
	}

	return result, fmt.Errorf("error %d %s", res.StatusCode, res.Status)
}

func (g *Grabber) FetchLive() ([]pinnacle.Live, error) {
	request := g.BuildRequest("sports/29/matchups/live", []map[string]string{
		{"withSpecials": "false"},
	})

	live, err := Fetch[[]pinnacle.Live](request)

	return live, err
}

func (g *Grabber) FetchLeagues() ([]pinnacle.League, error) {
	request := g.BuildRequest("sports/29/leagues", []map[string]string{
		{"all": "false"},
	})

	leagues, err := Fetch[[]pinnacle.League](request)

	return leagues, err
}

func (g *Grabber) FetchMatchUps(leagueID int64) ([]pinnacle.Match, error) {
	leaguePath := fmt.Sprintf("leagues/%d/matchups", leagueID)
	request := g.BuildRequest(leaguePath, nil)

	return Fetch[[]pinnacle.Match](request)
}

func (g *Grabber) FetchMarkets(leagueID int64) ([]pinnacle.MarketItem, error) {
	marketPath := fmt.Sprintf("leagues/%d/markets/straight", leagueID)
	request := g.BuildRequest(marketPath, nil)
	return Fetch[[]pinnacle.MarketItem](request)
}

func (g *Grabber) GetMarketMoneyLine(res []map[string]interface{}) ([]pinnacle.MarketItem, error) {
	markets := make([]pinnacle.MarketItem, 0)
	for _, moneyLine := range res {
		typeS, err := nestedmap.LookupString(moneyLine, "type")
		if err == nil {
			if typeS == pinnacle.MarketTypeMoneyLine {
				jsonBody, err := json.Marshal(moneyLine)
				if err != nil {
					return markets, err
				}
				var market pinnacle.MarketItem
				err = json.Unmarshal(jsonBody, &market)
				if err != nil {
					return markets, err
				}
				markets = append(markets, market)
			}
		}
	}
	return markets, nil
}

func (g *Grabber) MarketSpreadList(res []map[string]interface{}) ([]pinnacle.MarketItem, error) {
	markets := make([]pinnacle.MarketItem, 0)
	for _, spread := range res {
		typeS, err := nestedmap.LookupString(spread, "type")
		if err == nil {
			if typeS == pinnacle.MarketTypeSpread {
				jsonBody, err := json.Marshal(spread)
				if err != nil {
					return markets, err
				}
				var market pinnacle.MarketItem
				err = json.Unmarshal(jsonBody, &market)
				if err != nil {
					return markets, err
				}
				markets = append(markets, market)
			}
		}
	}
	return markets, nil
}

func (g *Grabber) PickMarkets(markets []pinnacle.MarketItem, matchUpID int64, lineType string) []pinnacle.MarketItem {
	spreads := make([]pinnacle.MarketItem, 0)
	for _, market := range markets {
		if market.MatchUpID == matchUpID && market.Type == lineType {
			spreads = append(spreads, market)
		}
	}
	return spreads
}
