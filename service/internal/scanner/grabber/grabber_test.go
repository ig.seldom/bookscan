package grabber

import (
	"fmt"
	"testing"

	"gitlab.com/ig.seldom/bookscan/internal/scanner/pinnacle"
)

func TestNewLeaguesRequest(t *testing.T) {
	config := AppConfig{}
	err := config.Load()
	grabber := New(config)
	if err != nil {
		t.Log(err)
		t.Fail()
		return
	}
	leagues, err := grabber.FetchLeagues()
	if err != nil {
		t.Log(err)
		t.Fail()
	}
	for _, league := range leagues {
		matchUps, err := grabber.FetchMatchUps(league.ID)
		if err != nil {
			t.Fail()
		}

		markets, err := grabber.FetchMarkets(league.ID)
		if err != nil {
			t.Fail()
		}

		for _, matchUp := range matchUps {
			if matchUp.Type == pinnacle.TypeMatchUp {
				home := ""
				away := ""
				for _, participant := range matchUp.Participants {
					if participant.Alignment == pinnacle.AlignmentAway {
						away = participant.Name
					}
					if participant.Alignment == pinnacle.AlignmentHome {
						home = participant.Name
					}
				}
				line := fmt.Sprintf("%s - %s %s, %v", home, away, league.Name, matchUp.StartTime)
				t.Log(line)
				spreads := grabber.PickMarkets(markets, matchUp.ID, "moneyline")
				for _, spread := range spreads {
					for _, price := range spread.Prices {
						points := fmt.Sprintf("%s %f %f", price.Designation, price.Points, price.DecimalPrice())
						t.Log(points)
					}
				}

			}
		}
	}
}
