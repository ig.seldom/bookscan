package grabber

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"

	"gitlab.com/ig.seldom/bookscan/internal/scanner/nestedmap"
)

type AppConfig struct {
	storage map[string]interface{}
}

func (a *AppConfig) GetString(path ...string) (string, error) {
	strI, err := nestedmap.Lookup(a.storage, path...)
	if err != nil {
		return "", err
	}
	str, ok := strI.(string)
	if !ok {
		return "", fmt.Errorf("Not found")
	}
	return str, nil
}

func (a *AppConfig) Load() error {
	location := &url.URL{
		Scheme: "https",
		Path:   "/config/app.json",
		Host:   "www.pinnacle.com",
	}
	client := http.Client{}
	request := &http.Request{
		Method: "GET",
		URL:    location,
	}

	res, err := client.Do(request)
	if err != nil {
		return err
	}
	defer func() {
		err = res.Body.Close()
		if err != nil {
			log.Println(err)
		}
	}()
	if res.StatusCode == http.StatusOK {
		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return err
		}
		a.storage = make(map[string]interface{}, 0)
		err = json.Unmarshal(body, &a.storage)
		if err != nil {
			return err
		}
		return nil
	}
	return fmt.Errorf("Error %d (%s)", res.StatusCode, res.Status)
}
