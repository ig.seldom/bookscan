package pinnacle

type Participant struct {
	Alignment string `json:"alignment"`
	Name      string `json:"name"`
	Order     int    `json:"order"`
}
