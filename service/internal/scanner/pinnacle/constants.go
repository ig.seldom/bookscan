package pinnacle

const (
	AlignmentHome       = "home"
	AlignmentAway       = "away"
	AlignmentDraw       = "draw"
	TypeMatchUp         = "matchup"
	MarketTypeSpread    = "spread"
	MarketTypeMoneyLine = "moneyline"
)
