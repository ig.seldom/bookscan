package pinnacle

import "testing"

func TestMarketSpreadPrice_DecimalPrice(t *testing.T) {
	prices := []MarketPrice{
		{
			Designation: "home",
			Points:      0,
			Price:       -165,
		}, {
			Designation: "away",
			Points:      0,
			Price:       146,
		},
	}
	for _, price := range prices {
		if price.Designation == "home" && price.DecimalPrice() != 1.606060606060606 {
			t.Log(price.DecimalPrice())
			t.Fail()
		}
		if price.Designation == "away" && price.DecimalPrice() != 2.46 {
			t.Log(price.DecimalPrice())
			t.Fail()
		}
	}
}
