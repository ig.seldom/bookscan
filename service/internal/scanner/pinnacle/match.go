package pinnacle

import "time"

type Match struct {
	AgeLimit          int64         `json:"ageLimit"`
	AltTeaser         bool          `json:"altTeaser"`
	External          interface{}   `json:"external"`
	HasAltSpread      bool          `json:"hasAltSpread"`
	HasAltTotal       bool          `json:"hasAltTotal"`
	HasLive           bool          `json:"hasLive"`
	HasMarkets        bool          `json:"hasMarkets"`
	ID                int64         `json:"id"`
	IsBetShareEnabled bool          `json:"isBetshareEnabled"`
	IsHighlighted     bool          `json:"isHighlighted"`
	IsLive            bool          `json:"isLive"`
	IsPromoted        bool          `json:"isPromoted"`
	League            League        `json:"league"`
	LiveMode          interface{}   `json:"liveMode"`
	Parent            interface{}   `json:"parent"`
	ParentID          *int64        `json:"parentId"`
	ParlayRestriction string        `json:"parlayRestriction"`
	Participants      []Participant `json:"participants"`
	Periods           []Period      `json:"periods"`
	Rotation          int64         `json:"rotation"`
	StartTime         time.Time     `json:"startTime"`
	State             interface{}   `json:"state"`
	Status            string        `json:"status"`
	TotalMarketCount  int64         `json:"totalMarketCount"`
	Type              string        `json:"type"`
	Units             string        `json:"units"`
	Version           int64         `json:"version"`
}
