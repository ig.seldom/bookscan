package pinnacle

import "time"

type Period struct {
	CutoffAt     time.Time `json:"cutoffAt"`
	HasMoneyLine bool      `json:"hasMoneyline"`
	HasSpread    bool      `json:"hasSpread"`
	HasTeamTotal bool      `json:"hasTeamTotal"`
	HasTotal     bool      `json:"hasTotal"`
	Period       int       `json:"period"`
	Status       string    `json:"status"`
}
