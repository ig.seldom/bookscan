package pinnacle

import (
	"math"
	"time"
)

type MarketItem struct {
	CutoffAt    time.Time           `json:"cutoffAt"`
	IsAlternate bool                `json:"isAlternate"`
	Key         string              `json:"key"`
	Limits      []MarketSpreadLimit `json:"limits"`
	MatchUpID   int64               `json:"matchupId"`
	Period      int64               `json:"period"`
	Prices      []MarketPrice       `json:"prices"`
	Type        string              `json:"type"`
	Version     int64               `json:"version"`
}

type MarketPrice struct {
	Designation string  `json:"designation"`
	Points      float64 `json:"points"`
	Price       int64   `json:"price"`
}

func (m MarketPrice) DecimalPrice() float32 {
	price := float64(m.Price)
	if price > 0 {
		return float32((price / 100) + 1)
	}
	return (100 / float32(math.Abs(price))) + 1
}

type MarketSpreadLimit struct {
	Amount int64  `json:"amount"`
	Type   string `json:"type"`
}
