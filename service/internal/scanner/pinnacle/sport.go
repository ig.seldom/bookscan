package pinnacle

type Sport struct {
	FeatureOrder      int64  `json:"featureOrder"`      //featureOrder	0
	ID                int64  `json:"id"`                //id	29
	IsFeatured        bool   `json:"isFeatured"`        //isFeatured	true
	IsHidden          bool   `json:"isHidden"`          //isHidden	false
	IsSticky          bool   `json:"isSticky"`          //isSticky	false
	MatchupCount      int64  `json:"matchupCount"`      //matchupCount	698
	MatchupCountSE    int64  `json:"matchupCountSE"`    //matchupCountSE	557
	Name              string `json:"name"`              //name	"Soccer"
	PrimaryMarketType string `json:"primaryMarketType"` //primaryMarketType	"moneyline"
}
