package pinnacle

type League struct {
	AgeLimit       int64       `json:"ageLimit"`       //ageLimit	0
	External       interface{} `json:"external"`       //external	{}
	FeatureOrder   int64       `json:"featureOrder"`   //featureOrder	-1
	Group          string      `json:"group"`          //group	"Finland"
	ID             int64       `json:"id"`             //id	2018
	IsFeatured     bool        `json:"isFeatured"`     //isFeatured	false
	IsHidden       bool        `json:"isHidden"`       //isHidden	false
	IsPromoted     bool        `json:"isPromoted"`     //isPromoted	false
	IsSticky       bool        `json:"isSticky"`       //isSticky	false
	MatchupCount   int64       `json:"matchupCount"`   //matchupCount	2
	MatchupCountSE int64       `json:"matchupCountSE"` //matchupCountSE	2
	Name           string      `json:"name"`           //name	"Finland - Kakkonen"
	Sport          Sport       `json:"sport"`
}
