package market

import (
	"gitlab.com/ig.seldom/bookscan/internal/scanner/background"
)

func SortByStart(m []*background.Market) []*background.Market {
	swap := func(ar []*background.Market, i, j int) {
		tmp := ar[i]
		ar[i] = ar[j]
		ar[j] = tmp
	}

	for i := 0; i < len(m); i++ {
		for j := i; j < len(m); j++ {
			if m[i].StartsAt.After(m[j].StartsAt) {
				swap(m, i, j)
			}
		}
	}

	return m
}

func LastPriceLowerThanFirst(m []*background.Market) []*background.Market {
	market := make([]*background.Market, 0)
	for _, item := range m {
		if item.HomePrice.First() < item.AwayPrice.Last() {
			market = append(market, item)
		}
	}
	return market
}

func Filter(m []*background.Market, filterFunc func(item background.Market) bool) []*background.Market {
	market := make([]*background.Market, 0)
	for _, item := range m {
		if filterFunc(*item) {
			market = append(market, item)
		}
	}
	return market
}
