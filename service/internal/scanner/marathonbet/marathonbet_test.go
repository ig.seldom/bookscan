package marathonbet

import (
	_ "embed"
	"testing"
	"time"
)

//go:embed page.json
var body string

func TestMarathon_parsePage(t *testing.T) {
	_, _, e := parseJSON([]byte(body))
	if e != nil {
		t.Log(e)
		t.FailNow()
	}
}

func TestMarathon_parseDate(t *testing.T) {
	now := time.Now()
	loc, err := time.LoadLocation("UTC")
	if err != nil {
		t.Log(err)
		t.FailNow()
	}
	dates := map[string]time.Time{
		"11 Aug 14:00": time.Date(now.Year(), time.August, 11, 14, 00, 00, 00, loc),
		"30 Sep 19:22": time.Date(now.Year(), time.September, 30, 19, 22, 00, 00, loc),
		"01 Oct 03:30": time.Date(now.Year(), time.October, 1, 3, 30, 00, 00, loc),
		"12 Nov 15:33": time.Date(now.Year(), time.November, 12, 15, 33, 00, 00, loc),
		"31 Dec 23:59": time.Date(now.Year(), time.December, 31, 23, 59, 00, 00, loc),
		"01 Jan 01:01": time.Date(now.Year(), time.January, 1, 1, 1, 00, 00, loc),
		"23:59":        time.Date(now.Year(), now.Month(), now.Day(), 23, 59, 00, 00, loc),
	}
	for np, date := range dates {
		d, err := ParseDate(np)
		if err != nil {
			t.Log(err)
			t.FailNow()
		}
		if time.Now().Month() > d.Month() {
			date = date.AddDate(1, 0, 0)
		}
		if d != date {
			t.Logf("Time %s must be %v given %v", np, date, d)
			t.Fail()
		}
	}

}

func TestMarathon_generateURL(t *testing.T) {
	url := generateURL(1)
	t.Log(url)
}
