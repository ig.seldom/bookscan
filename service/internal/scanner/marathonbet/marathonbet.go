package marathonbet

import (
	"errors"
	"fmt"
	"github.com/Jeffail/gabs"
	"io"
	"log"
	"net/http"
	"time"
)

type Marathon struct{}

type Page struct {
	Selector string `json:"selector"`
	Replace  string `json:"replace"`
	Content  string `json:"content"`
}

type Prop struct {
	Name  string `json:"prop"`
	Value bool   `json:"val"`
}

func retErr(err error) (string, bool, error) {
	return "", false, err
}

func generateURL(page uint8) string {
	ts := time.Now().UnixMilli()
	return fmt.Sprintf(
		"https://www.marathonbet.com/en/betting/Football+-+11?page=%d&pageAction=getPage&_=%d",
		page,
		ts,
	)
}

func cfHeaders(r *http.Request) *http.Request {
	h := map[string]string{
		"Host":             "www.marathonbet.com",
		"User-Agent":       "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:105.0) Gecko/20100101 Firefox/105.0",
		"Accept":           "application/json, text/javascript, */*; q=0.01",
		"Accept-Language":  "en-GB,en;q=0.5",
		"Accept-Encoding":  "gzip, deflate, br",
		"X-Requested-With": "XMLHttpRequest",
		"Connection":       "keep-alive",
		"Referer":          "https://www.marathonbet.com/en/betting/Football+-+11",
		"Cookie":           "puid=rBkp8mLIUu0VNDEYAwxpAg==; panbet.openeventnameseparately=true; panbet.openadditionalmarketsseparately=false; LIVE_TRENDS_STYLE=ARROW; panbet.oddstype=Decimal; lang=en; _gcl_au=1.1.128440584.1657295598; _dvp=0:l5cn0bvf:k_GsELYObnl8MffStiZWHJ5UnFrn_NAy; _ym_uid=1657295599419856922; _ym_d=1657295599; amplitude_id_c48cfa660f9302f2d63c30fba1f7f065_cwmarathonbet.com=eyJkZXZpY2VJZCI6IjkzMGQyZjY0LTQ0NDUtNDY0Ni1hZGU0LWUzN2M4MzYyZWZiMFIiLCJ1c2VySWQiOm51bGwsIm9wdE91dCI6ZmFsc2UsInNlc3Npb25JZCI6MTY2Mzg3NjQxNzgwOSwibGFzdEV2ZW50VGltZSI6MTY2Mzg3Nzk4MzYwOSwiZXZlbnRJZCI6MCwiaWRlbnRpZnlJZCI6MCwic2VxdWVuY2VOdW1iZXIiOjB9; _ga=GA1.2.853854044.1657295600; pagesViewed=11; viewedNotificationItems=HOME; visitedNavBarItems=HOME; SESSION_KEY=1bc1d1d31a4f4770a38426340abab6b7; _ym_isad=2; _gid=GA1.2.1296992897.1663858570; _dvs=0:l8dh24fu:rZqtPvOSzBKnkxUYRW3F88qwytVQElD7; JSESSIONID=web6~CB883DD31703C910BD0075AD18054D63",
		"Sec-Fetch-Dest":   "empty",
		"Sec-Fetch-Mode":   "cors",
		"Sec-Fetch-Site":   "same-origin",
		"TE":               "trailers",
	}
	for key, value := range h {
		r.Header.Set(key, value)
	}
	return r
}

func (m *Marathon) GetNextPage(page uint8) (string, bool, error) {
	c := &http.Client{}
	req, err := http.NewRequest("GET", generateURL(page), nil)
	if err != nil {
		return retErr(err)
	}
	req = cfHeaders(req)
	resp, err := c.Do(req)
	if err != nil {
		return retErr(err)
	}
	if resp.StatusCode != http.StatusOK {
		return retErr(errors.New(resp.Status))
	}
	defer func() {
		err := resp.Body.Close()
		if err != nil {
			log.Println(err)
		}
	}()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return retErr(err)
	}

	return parseJSON(body)
}

func parseJSON(body []byte) (string, bool, error) {
	var b []byte
	b = append(b, "{\"object\":"...)
	b = append(b, body...)
	b = append(b, "}"...)
	jsonParsed, err := gabs.ParseJSON(b)
	if err != nil {
		return retErr(err)
	}
	content, _ := jsonParsedType[string](jsonParsed, 0, "object", "content")
	hasNextPage, _ := jsonParsedType[bool](jsonParsed, 1, "object", "val")
	return content, hasNextPage, nil
}

func jsonParsedType[V string | bool](j *gabs.Container, i int, root, path string) (V, bool) {
	j, _ = j.ArrayElement(i, root)
	v, ok := j.Search(path).Data().(V)
	return v, ok
}

const ParseFormatDate = 0
const ParseFormatTime = 1

func parseDateByFormat(date string, formatType int) (time.Time, error) {
	format := "02 Jan 15:04"
	if formatType == ParseFormatTime {
		format = "15:04"
	}
	startsAt, err := time.Parse(format, date)
	if err != nil {
		return startsAt, err
	}
	loc, err := time.LoadLocation("UTC")
	if err != nil {
		return startsAt, err
	}
	startsAt = startsAt.In(loc)
	now := time.Now()

	if formatType == ParseFormatDate {
		year := now.Year()
		if now.Month() > startsAt.Month() {
			year++
		}
		startsAt = startsAt.AddDate(year, 0, 0).In(loc)
	} else if formatType == ParseFormatTime {
		startsAt = startsAt.AddDate(now.Year(), int(now.Month())-1, now.Day()-1).In(loc)
	} else {
		return startsAt, errors.New("unexpected parse format")
	}

	return startsAt, nil
}

func ParseDate(date string) (time.Time, error) {
	s, err := parseDateByFormat(date, ParseFormatDate)
	if err != nil {
		s, err = parseDateByFormat(date, ParseFormatTime)
	}
	return s, err
}
