package background

import (
	"time"
)

const (
	MatchProviderPinnacle = "pinnacle"
	MatchProviderMarathon = "marathon"
)

type Match struct {
	Home              string
	Away              string
	League            string
	StartsAt          time.Time
	HomePrice         Price
	AwayPrice         Price
	DrawPrice         Price
	Provider          string
	HandicapHomePrice Price
	HandicapAwayPrice Price
}

type (
	Price  float32
	Prices []Price
)

func (p *Prices) Last() Price {
	return (*p)[len(*p)-1]
}

func (p *Prices) First() Price {
	return (*p)[0]
}

type Market struct {
	Home              string    `json:"home"`
	Away              string    `json:"away"`
	League            string    `json:"league"`
	StartsAt          time.Time `json:"startsAt"`
	HomePrice         Prices    `json:"homePrice"`
	AwayPrice         Prices    `json:"awayPrice"`
	DrawPrice         Prices    `json:"drawPrice"`
	Provider          string    `json:"provider"`
	HandicapHomePrice Prices    `json:"handicapHomePrice"`
	HandicapAwayPrice Prices    `json:"handicapAwayPrice"`
}
