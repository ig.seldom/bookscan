package background

import (
	"context"
	"encoding/gob"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/ig.seldom/bookscan/internal/scanner/grabber"
	"gitlab.com/ig.seldom/bookscan/internal/scanner/marathonbet"
	"gitlab.com/ig.seldom/bookscan/internal/scanner/pinnacle"
	"gitlab.com/ig.seldom/bookscan/internal/server/utils"
)

type Config struct {
	DumpFileName string
}

type FetchTask struct {
	Config      Config
	Grabber     *grabber.Grabber
	Marathon    *marathonbet.Marathon
	Market      map[string]*Market
	MarketSlice []*Market
	LockMarket  sync.RWMutex
	CreateChan  chan Market
	UpdateChan  chan Market
	ctx         context.Context
}

func (f *FetchTask) Init(dumpFileName string, ctx context.Context) error {
	var err error
	f.ctx = ctx
	f.MarketSlice = make([]*Market, 0)
	f.Market = make(map[string]*Market)
	f.CreateChan = make(chan Market)
	f.UpdateChan = make(chan Market)
	f.Config.DumpFileName = dumpFileName
	f.LoadFromFile()
	config := grabber.AppConfig{}
	err = config.Load()
	if err != nil {
		return err
	}
	f.Grabber = grabber.New(config)
	f.Marathon = &marathonbet.Marathon{}
	return nil
}

// Start starts the parsing job
func (f *FetchTask) Start(interval int64) error {
	go func() {
		var running atomic.Bool
		running.Store(true)
		go func() {
			<-f.ctx.Done()
			running.Store(false)
		}()
		for running.Load() {
			err := f.Job()
			if err != nil {
				log.Println(err)
			}
			time.Sleep(time.Duration(interval) * time.Second)
		}
	}()

	go func() {
		var running atomic.Bool
		running.Store(true)
		go func() {
			<-f.ctx.Done()
			running.Store(false)
		}()
		for running.Load() {
			err := f.Live()
			if err != nil {
				log.Println(err)
			}
			time.Sleep(time.Duration(interval) * time.Second)
		}
	}()
	return nil
}

func (f *FetchTask) Live() error {
	return nil
}

func (f *FetchTask) Job() error {
	leagues, err := f.Grabber.FetchLeagues()
	if err != nil {
		log.Println(err)
	}

	for _, league := range leagues {
		matchUps, err := f.Grabber.FetchMatchUps(league.ID)
		if err != nil {
			log.Println(err)
		}

		markets, err := f.Grabber.FetchMarkets(league.ID)
		if err != nil {
			log.Println(err)
		}

		for _, matchUp := range matchUps {
			if matchUp.Type == pinnacle.TypeMatchUp {
				home := ""
				away := ""
				for _, participant := range matchUp.Participants {
					switch participant.Alignment {
					case pinnacle.AlignmentHome:
						home = participant.Name
					case pinnacle.AlignmentAway:
						away = participant.Name
					}
				}

				if strings.Contains(home, "Corners") || strings.Contains(away, "Corners") {
					continue
				}

				mm := Match{
					Home:     home,
					Away:     away,
					League:   league.Name,
					StartsAt: utils.AlignTimezone(matchUp.StartTime),
					Provider: MatchProviderPinnacle,
				}

				if marketItems := f.Grabber.PickMarkets(markets, matchUp.ID, "spread"); len(marketItems) > 0 {
					for _, marketItem := range marketItems {
						for _, price := range marketItem.Prices {
							if price.Points == 0 {
								switch price.Designation {
								case pinnacle.AlignmentHome:
									mm.HandicapHomePrice = Price(price.DecimalPrice())
								case pinnacle.AlignmentAway:
									mm.HandicapAwayPrice = Price(price.DecimalPrice())
								}
							}
						}
					}
				}

				if marketItems := f.Grabber.PickMarkets(markets, matchUp.ID, "moneyline"); len(marketItems) > 0 {
					var homePrice, awayPrice, drawPrice Price
					for _, marketItem := range marketItems {
						for _, price := range marketItem.Prices {
							if price.Points == 0 {
								switch price.Designation {
								case pinnacle.AlignmentHome:
									homePrice = Price(price.DecimalPrice())
								case pinnacle.AlignmentAway:
									awayPrice = Price(price.DecimalPrice())
								case pinnacle.AlignmentDraw:
									drawPrice = Price(price.DecimalPrice())
								}
							}
						}
					}
					if homePrice > 0 && awayPrice > 0 && drawPrice > 0 {
						mm.HomePrice = homePrice
						mm.AwayPrice = awayPrice
						mm.DrawPrice = drawPrice
						f.Compare(mm)
						f.DumpToFile()
					} else {
						log.Printf("Match [%s] %s - %s has 0 price value", league.Name, home, away)
					}
				}
			}
		}
	}

	return nil
}

func (f *FetchTask) Compare(newMatch Match) {
	f.RemoveStarted()
	f.LockMarket.Lock()
	defer f.LockMarket.Unlock()
	key := fmt.Sprintf("%s-%s-%s-%s",
		newMatch.Home,
		newMatch.Away,
		newMatch.League,
		newMatch.Provider,
	)
	if match, found := f.Market[key]; found {
		lastHomePrice := match.HomePrice[len(match.HomePrice)-1]
		lastAwayPrice := match.AwayPrice[len(match.AwayPrice)-1]
		if lastHomePrice != newMatch.HomePrice || lastAwayPrice != newMatch.AwayPrice {
			match.StartsAt = newMatch.StartsAt
			match.HomePrice = append(match.HomePrice, newMatch.HomePrice)
			match.AwayPrice = append(match.AwayPrice, newMatch.AwayPrice)
			match.DrawPrice = append(match.DrawPrice, newMatch.DrawPrice)
			match.Provider = MatchProviderPinnacle
			match.HandicapAwayPrice = append(match.HandicapAwayPrice, newMatch.HandicapAwayPrice)
			match.HandicapHomePrice = append(match.HandicapHomePrice, newMatch.HandicapAwayPrice)
			f.UpdateChan <- *match
		}
	} else {
		match := &Market{
			Home:     newMatch.Home,
			Away:     newMatch.Away,
			League:   newMatch.League,
			StartsAt: newMatch.StartsAt,
		}
		match.HomePrice = append(match.HomePrice, newMatch.HomePrice)
		match.AwayPrice = append(match.AwayPrice, newMatch.AwayPrice)
		match.DrawPrice = append(match.DrawPrice, newMatch.DrawPrice)
		match.HandicapAwayPrice = append(match.HandicapAwayPrice, newMatch.HandicapAwayPrice)
		match.HandicapHomePrice = append(match.HandicapHomePrice, newMatch.HandicapAwayPrice)
		match.Provider = MatchProviderPinnacle
		f.Market[key] = match
		f.CreateChan <- *match
	}
}

func (f *FetchTask) DumpToFile() *FetchTask {
	f.LockMarket.RLock()
	defer f.LockMarket.RUnlock()

	if file, err := os.Create(f.Config.DumpFileName); err == nil {
		e := gob.NewEncoder(file)
		if err := e.Encode(f.Market); err != nil {
			log.Println(err)
		}
		file.Close()
	} else {
		log.Println(err)
	}

	return f
}

func (f *FetchTask) LoadFromFile() *FetchTask {
	f.LockMarket.Lock()
	defer f.LockMarket.Unlock()

	if file, err := os.Open(f.Config.DumpFileName); err == nil {
		d := gob.NewDecoder(file)
		if err := d.Decode(&f.Market); err != nil {
			log.Println("market dump not used err = ", err)
		}
		file.Close()
	} else {
		log.Println(err)
	}
	return f
}

func (f *FetchTask) MapToSlice() *FetchTask {
	f.LockMarket.RLock()
	defer f.LockMarket.RUnlock()
	f.MarketSlice = make([]*Market, len(f.Market))
	var i int64
	for _, item := range f.Market {
		f.MarketSlice[i] = item
		i++
	}
	return f
}

func (f *FetchTask) Before(b time.Duration) []*Market {
	market := make([]*Market, 0)
	beforeDate := time.Now().Add(b)
	for _, item := range f.Market {
		if item.StartsAt.Before(beforeDate) {
			market = append(market, item)
		}
	}
	return market
}

func (f *FetchTask) GetMarket() []*Market {
	return f.RemoveStarted().MapToSlice().MarketSlice
}

func (f *FetchTask) GetLive() (pinnacle.Live, error) {
	return f.Grabber.FetchLive()
}

func (f *FetchTask) RemoveStarted() *FetchTask {
	f.LockMarket.Lock()
	defer f.LockMarket.Unlock()
	for key, item := range f.Market {
		now := utils.TimeNow()
		if item.StartsAt.Before(now) {
			delete(f.Market, key)
		}
	}
	return f
}

func (f *FetchTask) MarathonJob() error {
	var page uint8 = 0
	hasNextPage := true
	var content string
	var err error
	for {
		if hasNextPage {
			content, hasNextPage, err = f.Marathon.GetNextPage(page)
			if err != nil {
				return err
			}
			page++
			_, err := f.MarathonParse(content)
			if err != nil {
				return err
			}
		} else {
			break
		}
	}
	return err
}

func (f *FetchTask) MarathonParse(content string) ([]Match, error) {
	retErr := func(err error) ([]Match, error) {
		return nil, err
	}

	doc, err := goquery.NewDocumentFromReader(strings.NewReader(content))
	if err != nil {
		return retErr(err)
	}

	doc.Find("div .category-container").Each(func(i int, s *goquery.Selection) {
		s.Find(".category-content .coupon-row .sub-row").Each(func(i int, s *goquery.Selection) {
			m := Match{}
			m.League = strings.TrimSpace(s.Find(".category-container .category-label-td .category-label").Text())
			m.Provider = MatchProviderMarathon
			m.Home = strings.TrimSpace(s.Find(".member-area-content-table tr:nth-child(1) .member-link").Text())
			m.Away = strings.TrimSpace(s.Find(".member-area-content-table tr:nth-child(2) .member-link").Text())
			startsAtString := strings.TrimSpace(s.Find(".member-area-content-table tr:nth-child(1) .date-short").Text())
			m.StartsAt, err = marathonbet.ParseDate(startsAtString)
			if err != nil {
				log.Println(err)
			}
			hp, err := strconv.ParseFloat(strings.TrimSpace(s.Find("td:nth-child(3) span").Text()), 32)
			if err != nil {
				log.Println(err)
			}
			dp, err := strconv.ParseFloat(strings.TrimSpace(s.Find("td:nth-child(4) span").Text()), 32)
			if err != nil {
				log.Println(err)
			}
			ap, err := strconv.ParseFloat(strings.TrimSpace(s.Find("td:nth-child(5) span").Text()), 32)
			if err != nil {
				log.Println(err)
			}

			m.HomePrice = Price(hp)
			m.DrawPrice = Price(dp)
			m.AwayPrice = Price(ap)
			f.Compare(m)
		})
	})
	return nil, nil
}
