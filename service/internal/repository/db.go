package repository

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

// db instance
var db *sqlx.DB

// Connect to db
func Connect(dataSourceName string) error {
	var err error
	db, err = sqlx.Open("postgres", dataSourceName)
	if err != nil {
		return fmt.Errorf("could not open a connection with the database\n%v", err)
	}
	if err := db.Ping(); err != nil {
		return fmt.Errorf("could not establish a connection with the database\n%v", err)
	}
	return nil
}

// Disconnect disconnects from DB
func Disconnect() error {
	return db.Close()
}
