package repository

import (
	"context"
	"database/sql"
	"time"

	"gitlab.com/ig.seldom/bookscan/internal/repository/models"
	"gitlab.com/ig.seldom/bookscan/internal/scanner/background"
)

func UpdateUserPassword(resetPasswordID int, password string) error {
	_, err := db.Query(updateUserPasswordQuery, resetPasswordID, password)
	return err
}

func CreateUser(password, email string) error {
	_, err := db.Query(createUserQuery, password, email)
	return err
}

func Login(email string) *sql.Row {
	return db.QueryRow(loginQuery, email)
}

func CheckUserExists(email string) (*sql.Rows, error) {
	return db.Query(checkUserExistsQuery, email)
}

func GetBettingMatchWithInsert(ctx context.Context, m background.Match) (models.Match, error) {
	var match models.Match
	league, err := GetBettingLeagueWithInsert(ctx, m.League)
	if err != nil {
		return match, err
	}
	home, err := GetBettingParticipantWithInsert(ctx, m.Home)
	if err != nil {
		return match, err
	}
	away, err := GetBettingParticipantWithInsert(ctx, m.Away)
	if err != nil {
		return match, err
	}
	match.League = league
	match.Home = home
	match.Away = away
	return match, nil
}

func CreateBettingMatchAndGet(ctx context.Context, league, home, away string, startedAt time.Time) (models.Match, error) {
	match, err := GetBettingMatchByName(ctx, league, home, away, startedAt)
	if err == sql.ErrNoRows {
		err = CreateBettingMatch(ctx, league, home, away, startedAt)
		if err != nil {
			return match, err
		}
		return GetBettingMatchByName(ctx, league, home, away, startedAt)
	}
	return match, err
}

func CreateBettingMatch(ctx context.Context, league, home, away string, startedAt time.Time) error {
	mLeague, err := GetBettingLeagueWithInsert(ctx, league)
	if err != nil {
		return err
	}
	mHome, err := GetBettingParticipantWithInsert(ctx, home)
	if err != nil {
		return err
	}
	mAway, err := GetBettingParticipantWithInsert(ctx, away)
	if err != nil {
		return err
	}
	return InsertBettingMatch(ctx, mHome.ID, mAway.ID, mLeague.ID, startedAt)
}

func InsertBettingMatch(ctx context.Context, homeID, awayID, leagueID models.ModelID, startedAt time.Time) error {
	_, err := db.ExecContext(
		ctx,
		"INSERT INTO match_table(home_id, away_id, league_id, started_at, created_at) VALUES($1, $2, $3, $4, NOW())",
		homeID, awayID, leagueID, startedAt,
	)

	return err
}

func GetModel[T any](ctx context.Context, selectSQL string, args ...any) (T, error) {
	var model T
	err := db.GetContext(ctx, &model, selectSQL, args...)
	return model, err
}

func GetBettingMatchByName(ctx context.Context, league, home, away string, startedAt time.Time) (models.Match, error) {
	var match models.Match
	mLeague, err := GetModel[models.League](ctx, "SELECT id, name, created_at, updated_at FROM leagues WHERE name = $1", league)
	if err != nil {
		return match, err
	}
	mHome, err := GetParticipant(ctx, home)
	if err != nil {
		return match, err
	}
	mAway, err := GetParticipant(ctx, away)
	if err != nil {
		return match, err
	}
	match, err = GetModel[models.Match](
		ctx,
		"SELECT id, home_id, away_id, league_id, started_at, created_at FROM match_table WHERE home_id = $1 AND away_id = $2 AND league_id = $3 AND started_at = $4",
		mHome.ID, mAway.ID, mLeague.ID, startedAt,
	)

	return match, err
}

func GetCountMatchNotifyByChatIDAndMatchID(ctx context.Context, chatID int64, matchID models.ModelID) (int, error) {
	count := 0
	err := db.GetContext(ctx, &count, "SELECT COUNT(id) FROM match_notify where chat_id = $1 AND match_id = $2", chatID, matchID)
	return count, err
}

func InsertMatchNotify(ctx context.Context, chatID int64, matchID models.ModelID) error {
	_, err := db.ExecContext(ctx, "INSERT INTO match_notify(chat_id, match_id, created_at) VALUES($1, $2, NOW())", chatID, matchID)
	return err
}

func GetBotChatActive(ctx context.Context) ([]models.BotChat, error) {
	return GetModels[models.BotChat](ctx, "SELECT id, chat_id, active, created_at, updated_at FROM bot_chats WHERE active = $1", true)
}

func GetModels[T any](ctx context.Context, selectSQL string, args ...any) ([]T, error) {
	var models []T
	rows, err := db.QueryxContext(ctx, selectSQL, args...)
	if err != nil {
		return models, err
	}
	for rows.Next() {
		var m T
		err := rows.StructScan(&m)
		if err != nil {
			return models, err
		}
		models = append(models, m)
	}
	return models, nil
}

func InsertBotChatUnique(ctx context.Context, chatID int64) error {
	count := 0
	err := db.Get(&count, "SELECT COUNT(id) FROM bot_chats WHERE chat_id = $1", chatID)
	if err != nil {
		return err
	}
	if count == 0 {
		_, err = db.ExecContext(ctx, "INSERT INTO bot_chats(chat_id, active, created_at, updated_at) VALUES($1, false, NOW(), NOW())", chatID)
	}
	return err
}

func GetParticipant(ctx context.Context, name string) (models.Participant, error) {
	return GetModel[models.Participant](ctx, "SELECT id, name, created_at, updated_at FROM participants WHERE name = $1", name)
}

func InsertBettingMatchPrice(ctx context.Context, matchID models.ModelID, home, away, draw background.Price) error {
	_, err := db.ExecContext(
		ctx,
		"INSERT INTO match_prices(match_id, home, away, draw, price_type, created_at) VALUES($1, $2, $3, $4, 'moneyline', NOW())",
		matchID, home, away, draw,
	)

	return err
}

func GetBettingLeagueWithInsert(ctx context.Context, name string) (models.League, error) {
	return GetModelWithInsert[models.League](
		ctx,
		"SELECT id, name, created_at, updated_at FROM leagues WHERE name = $1",
		"INSERT INTO leagues(name, created_at, updated_at) VALUES($1, NOW(), NOW()) RETURNING id, name, created_at, updated_at",
		name,
	)
}

func GetBettingParticipantWithInsert(ctx context.Context, name string) (models.Participant, error) {
	return GetModelWithInsert[models.Participant](
		ctx,
		"SELECT id, name, created_at, updated_at FROM participants WHERE name = $1",
		"INSERT INTO participants(name, created_at, updated_at) VALUES($1, NOW(), NOW()) RETURNING id, name, created_at, updated_at",
		name,
	)
}

func GetModelWithInsert[T any](ctx context.Context, selectSQL, insertSQL string, args ...any) (T, error) {
	var model T

	err := db.GetContext(ctx, &model, selectSQL, args...)
	if err == sql.ErrNoRows {
		err := db.GetContext(ctx, &model, insertSQL, args...)
		if err != nil {
			return model, err
		}
	} else if err != nil {
		return model, err
	}

	return model, nil
}
