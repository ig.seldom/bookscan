package repository

const (
	checkUserExistsQuery    = `SELECT id from users WHERE email = $1`
	loginQuery              = `SELECT * from users WHERE email = $1`
	createUserQuery         = `INSERT INTO users(id,password,email) VALUES (DEFAULT, $1, $2);`
	updateUserPasswordQuery = `UPDATE users SET password = $2 WHERE id = $1`
)
