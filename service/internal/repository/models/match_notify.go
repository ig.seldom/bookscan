package models

type MatchNotify struct {
	ID      ModelID `db:"id"`
	ChatID  int64   `db:"chat_id"`
	MatchID ModelID `db:"chat_id"`
}
