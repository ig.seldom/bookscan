package models

import "time"

type ModelID int32

type League struct {
	ID        ModelID   `db:"id"`
	Name      string    `db:"name"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
}

type Participant struct {
	ID        ModelID   `db:"id"`
	Name      string    `db:"name"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
}

type Match struct {
	ID        ModelID   `db:"id"`
	HomeID    ModelID   `db:"home_id"`
	AwayID    ModelID   `db:"away_id"`
	LeagueID  ModelID   `db:"league_id"`
	StartedAt time.Time `db:"started_at"`
	CreatedAt time.Time `db:"created_at"`
	Home      Participant
	Away      Participant
	League    League
}
