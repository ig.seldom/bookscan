package models

import "time"

type BotChat struct {
	ID        ModelID   `db:"id"`
	ChatID    int64     `db:"chat_id"`
	Active    bool      `db:"active"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
}
