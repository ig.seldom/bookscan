package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// GetMarket return set of market
func GetMarket(c *gin.Context) {
	_, isAuthenticated := AuthMiddleware(c, jwtKey)
	if !isAuthenticated {
		c.JSON(http.StatusUnauthorized, gin.H{"success": false, "msg": "unauthorized"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "market": app.GetMarket()})
}
