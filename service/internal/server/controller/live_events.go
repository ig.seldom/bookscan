package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// GetMarket return set of market
func GetLive(c *gin.Context) {
	_, isAuthenticated := AuthMiddleware(c, jwtKey)
	if !isAuthenticated {
		c.JSON(http.StatusUnauthorized, gin.H{"success": false, "msg": "unauthorized"})
		return
	}
	live, err := app.GetLive()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"success": false, "live": live})
	}
	c.JSON(http.StatusOK, gin.H{"success": true, "live": live})
}
