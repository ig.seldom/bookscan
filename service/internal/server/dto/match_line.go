package dto

import "time"

type Price struct {
	Home float64 `json:"home"`
	Away float64 `json:"away"`
	Draw float64 `json:"draw"`
}

type MatchLine struct {
	ID       int64     `json:"id"`
	League   string    `json:"league"`
	Home     string    `json:"home"`
	Away     string    `json:"away"`
	StartsAt time.Time `json:"startsAt"`
	Prices   []Price   `json:"prices"`
}
