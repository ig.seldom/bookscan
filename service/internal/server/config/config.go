package config

import "os"

var (
	ClientURL = os.Getenv("CLIENT_URL")
	TimeZone  = os.Getenv("TZ")
)
