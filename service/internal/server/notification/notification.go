package notification

import (
	"context"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type Notfier struct {
	bot        *tgbotapi.BotAPI
	ChatIDChan chan int64
}

func (n *Notfier) create(token string) error {
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		return err
	}
	n.ChatIDChan = make(chan int64)
	n.bot = bot
	return nil
}

func NewNotifier(token string) (*Notfier, error) {
	n := Notfier{}
	err := n.create(token)
	return &n, err
}

func (n *Notfier) Send(text string, chatID int64) error {
	msg := tgbotapi.NewMessage(chatID, text)
	_, err := n.bot.Send(msg)
	return err
}

func (n *Notfier) GetUpdatesChan(ctx context.Context) tgbotapi.UpdatesChannel {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	return n.bot.GetUpdatesChan(u)
}
