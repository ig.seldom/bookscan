package utils

import (
	"log"
	"os"
	"time"
)

var externalIPTimeLocation *time.Location
var serviceTimeLocation *time.Location
var logger log.Logger

func init() {
	var err error
	externalIPTimezoneName := os.Getenv("EXTERNAL_IP_TIMEZONE")
	if len(externalIPTimezoneName) == 0 {
		externalIPTimezoneName = "Europe/Berlin"
	}
	if externalIPTimeLocation, err = time.LoadLocation(externalIPTimezoneName); err != nil {
		logger.Println(err)
	}
	serviceTimezoneName := os.Getenv("SERVICE_TIMEZONE")
	if len(serviceTimezoneName) == 0 {
		serviceTimezoneName = "Europe/Moscow"
	}
	if serviceTimeLocation, err = time.LoadLocation(serviceTimezoneName); err != nil {
		logger.Println(err)
	}
}

func AlignTimezone(t time.Time) time.Time {
	t = t.In(externalIPTimeLocation)
	return t.In(serviceTimeLocation)
}

func TimeNow() time.Time {
	return time.Now().In(serviceTimeLocation)
}
