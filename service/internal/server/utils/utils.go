package utils

import (
	"regexp"

	"gitlab.com/ig.seldom/bookscan/internal/constants"
	"gitlab.com/ig.seldom/bookscan/internal/form"
	"gitlab.com/ig.seldom/bookscan/internal/server/db"
)

const (
	emailRegex = `^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`
)

// ValidateUser returns a slice of string of validation errors
func ValidateUser(user form.SignUp, err []string) []string {
	emailCheck := regexp.MustCompile(emailRegex).MatchString(user.Email)
	if !emailCheck {
		err = append(err, "Invalid email")
	}
	if len(user.Password) < 4 {
		err = append(err, "Invalid password, Password should be more than 4 characters")
	}

	if user.Password != user.PasswordRepeat {
		err = append(err, "passwords not match")
	}

	if user.AcceptTermsAndConditions != constants.TermsAndConditionsAccepted {
		err = append(err, "you should accept terms and conditions")
	}

	return err
}

func ValidatePasswordReset(resetPassword db.ResetPassword) (bool, string) {
	if len(resetPassword.Password) < 4 {
		return false, "Invalid password, password should be more than 4 characters"
	}
	if resetPassword.Password != resetPassword.ConfirmPassword {
		return false, "Password reset failed, passwords must match"
	}
	return true, ""
}
