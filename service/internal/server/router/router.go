package router

import (
	"gitlab.com/ig.seldom/bookscan/internal/server/controller"
	"gitlab.com/ig.seldom/bookscan/internal/server/middlewares"

	"github.com/gin-gonic/gin"
)

// SetupRouter setup routing here
func SetupRouter() *gin.Engine {
	router := gin.Default()

	// Middlewares
	router.Use(middlewares.ErrorHandler)
	router.Use(middlewares.CORSMiddleware())
	// routes
	apiGroup := router.Group("/api")
	{
		apiGroup.GET("/ping", controller.Pong)
		apiGroup.POST("/register", controller.Create)
		apiGroup.POST("/login", controller.Login)
		apiGroup.GET("/live", controller.GetLive)
		apiGroup.GET("/market", controller.GetMarket)
		apiGroup.POST("/createReset", controller.InitiatePasswordReset)
		apiGroup.POST("/resetPassword", controller.ResetPassword)
	}
	return router
}
