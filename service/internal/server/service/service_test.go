package service

import (
	"testing"
	"time"

	"gitlab.com/ig.seldom/bookscan/internal/scanner/background"
)

func TestGetMarket(t *testing.T) {
	tableTests := []struct {
		Name   string
		Length int
		Market []*background.Market
	}{
		{
			"Not filterable",
			1,
			[]*background.Market{{
				Home:              "Home",
				Away:              "Away",
				League:            "Not filterable",
				StartsAt:          time.Now().Add(time.Hour * 1),
				HomePrice:         background.Prices{2.4, 2.32, 2.21},
				DrawPrice:         background.Prices{3.4, 3.45, 3.6},
				AwayPrice:         background.Prices{4.5, 4.6, 4.71},
				Provider:          "testing provider",
				HandicapHomePrice: background.Prices{1.66},
			}},
		},
		{
			"Filtered by starts at",
			0,
			[]*background.Market{{
				Home:              "Home",
				Away:              "Away",
				League:            "Not filterable",
				StartsAt:          time.Now().Add(time.Hour * 1),
				HomePrice:         background.Prices{2.43, 2.32, 2.21},
				AwayPrice:         background.Prices{3.11, 3.22, 3.33},
				DrawPrice:         background.Prices{4.44, 4.55, 4.66},
				Provider:          "testing provider",
				HandicapHomePrice: background.Prices{1.66},
			}},
		},
		{
			"Filtered by price in range",
			0,
			[]*background.Market{{
				Home:              "Home 2",
				Away:              "Away 2",
				League:            "Filtered by price in range",
				StartsAt:          time.Now().Add(time.Hour * 2),
				HomePrice:         background.Prices{1.43, 1.32, 1.21},
				AwayPrice:         background.Prices{3.11, 3.22, 3.33},
				DrawPrice:         background.Prices{4.44, 4.55, 4.66},
				Provider:          "testing provider",
				HandicapHomePrice: background.Prices{1.66},
			}},
		},
		{
			"Filtered by last lower than first",
			0,
			[]*background.Market{{
				Home:              "Home 2",
				Away:              "Away 2",
				League:            "Filtered by last lower than first",
				StartsAt:          time.Now().Add(time.Hour * 2),
				HomePrice:         background.Prices{2.43, 1.32, 3.21},
				AwayPrice:         background.Prices{3.11, 3.22, 3.33},
				DrawPrice:         background.Prices{4.44, 4.55, 4.66},
				Provider:          "testing provider",
				HandicapHomePrice: background.Prices{1.66},
			}},
		},
		{
			"Filtered by more than one",
			0,
			[]*background.Market{{
				Home:              "Home 2",
				Away:              "Away 2",
				League:            "Filtered by more than one",
				StartsAt:          time.Now().Add(time.Hour * 2),
				HomePrice:         background.Prices{2.43},
				AwayPrice:         background.Prices{3.11},
				DrawPrice:         background.Prices{4.44},
				Provider:          "testing provider",
				HandicapHomePrice: background.Prices{1.66},
			}},
		},
		{
			"Filtered last prices ascending",
			0,
			[]*background.Market{{
				Home:              "Home 2",
				Away:              "Away 2",
				League:            "Filtered last prices ascending",
				StartsAt:          time.Now().Add(time.Hour * 2),
				HomePrice:         background.Prices{2.43, 1.32, 3.21},
				AwayPrice:         background.Prices{3.11, 3.22, 4.66},
				DrawPrice:         background.Prices{4.44, 4.55, 3.33},
				Provider:          "testing provider",
				HandicapHomePrice: background.Prices{1.66},
			}},
		},
		{
			"Filtered max price is first",
			0,
			[]*background.Market{{
				Home:              "Home 2",
				Away:              "Away 2",
				League:            "League",
				StartsAt:          time.Now().Add(time.Hour * 2),
				HomePrice:         background.Prices{2.43, 2.44, 3.21},
				AwayPrice:         background.Prices{3.11, 3.22, 4.66},
				DrawPrice:         background.Prices{4.44, 4.55, 3.33},
				Provider:          "testing provider",
				HandicapHomePrice: background.Prices{1.66},
			}},
		},
	}

	for _, test := range tableTests {
		m := getMarket(test.Market)
		l := len(m)
		if l != test.Length {
			t.Logf("%s length expected to be %d got %d\n", test.Name, test.Length, l)
			t.Fail()
		}
	}
}
