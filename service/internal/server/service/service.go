package service

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/ig.seldom/bookscan/internal/repository"
	"gitlab.com/ig.seldom/bookscan/internal/scanner/background"
	"gitlab.com/ig.seldom/bookscan/internal/scanner/market"
	"gitlab.com/ig.seldom/bookscan/internal/scanner/pinnacle"
	"gitlab.com/ig.seldom/bookscan/internal/server/notification"
)

func NewService(ctx context.Context, notifier *notification.Notfier) *service {
	return &service{ctx: ctx, notifier: notifier}
}

type service struct {
	scanner  *background.FetchTask
	ctx      context.Context
	notifier *notification.Notfier
}

type Service interface {
	RunScanner() error
	GetMarket() []*background.Market
	GetLive() (pinnacle.Live, error)
	ConnectToDB(string) error
	ListenChatSubscribers()
	NotifySubscribers()
	DisconnectFromDB() error
}

func (s *service) NotifySubscribers() {
	go func() {
		for {
			select {
			case <-s.ctx.Done():
				return
			case <-time.After(time.Minute * 1):
				m := getMarket(s.scanner.GetMarket())
				for _, item := range m {
					match, err := repository.CreateBettingMatchAndGet(s.ctx, item.League, item.Home, item.Away, item.StartsAt)
					if err != nil {
						log.Println("Notification send get match error", err)
						break
					}
					chats, err := repository.GetBotChatActive(s.ctx)
					if err != nil {
						log.Println("Notification send get chats error")
						break
					}
					messageText := fmt.Sprintf(
						"New match available to observe!\n\n%s\n%s vs %s\n%.2f ... %.2f\n%.2f ... %.2f\n%.2f ... %.2f\nBegins at %s",
						item.League,
						item.Home,
						item.Away,
						item.HomePrice.First(),
						item.HomePrice.Last(),
						item.DrawPrice.First(),
						item.DrawPrice.Last(),
						item.AwayPrice.First(),
						item.AwayPrice.Last(),
						item.StartsAt.String(),
					)
					for _, chat := range chats {
						count, err := repository.GetCountMatchNotifyByChatIDAndMatchID(s.ctx, chat.ChatID, match.ID)
						if err != nil {
							log.Println("Notification send error can't get count notifications", err)
						}
						if count == 0 {
							if err := s.notifier.Send(messageText, chat.ChatID); err != nil {
								log.Println("Can't send message", err)
							}
							if err := repository.InsertMatchNotify(s.ctx, chat.ChatID, match.ID); err != nil {
								log.Println("Can't save notification", err)
							}
						}
					}
				}
			}
		}
	}()
}

func (s *service) ListenChatSubscribers() {
	updates := s.notifier.GetUpdatesChan(s.ctx)
	go func() {
		for {
			select {
			case update := <-updates:
				if update.Message != nil {
					if err := repository.InsertBotChatUnique(s.ctx, update.Message.Chat.ID); err != nil {
						log.Println(err)
					}
				}
			case <-s.ctx.Done():
				return
			}
		}
	}()
}

func (s *service) RunScanner() error {
	dumpFileName := os.Getenv("DUMP_FILE_NAME")
	if dumpFileName == "" {
		dumpFileName = "./market.bin"
	}
	s.scanner = &background.FetchTask{}
	err := s.scanner.Init(dumpFileName, s.ctx)
	if err != nil {
		return err
	}
	err = s.scanner.Start(10)
	if err != nil {
		return err
	}

	go func() {
		for {
			select {
			case create := <-s.scanner.CreateChan:
				s.CreateBettingMatch(create)
			case update := <-s.scanner.UpdateChan:
				s.UpdateBettingMatch(update)
			case <-s.ctx.Done():
				log.Println("Shutting down...")
				return
			}
		}
	}()
	return nil
}

func (s *service) UpdateBettingMatch(m background.Market) {
	match, err := repository.CreateBettingMatchAndGet(s.ctx, m.League, m.Home, m.Away, m.StartsAt)
	if err != nil {
		log.Println("Failed to create match", err)
		return
	}

	if err == nil {
		if err := repository.InsertBettingMatchPrice(s.ctx, match.ID, m.HomePrice.Last(), m.AwayPrice.Last(), m.DrawPrice.Last()); err != nil {
			log.Printf("Failed to insert match prices %s %s %s at %s error(%v)\n", m.League, m.Home, m.Away, m.StartsAt.String(), err)
			return
		}
	} else {
		log.Printf("Failed to get match %s %s %s at %s error(%v)\n", m.League, m.Home, m.Away, m.StartsAt.String(), err)
		return
	}
}

func (s *service) CreateBettingMatch(m background.Market) {
	if match, err := repository.CreateBettingMatchAndGet(s.ctx, m.League, m.Home, m.Away, m.StartsAt); err != nil {
		log.Printf("Failed to insert match %s %s %s at %s error(%v)\n", m.League, m.Home, m.Away, m.StartsAt.String(), err)
		return
	} else {
		if err := repository.InsertBettingMatchPrice(s.ctx, match.ID, m.HomePrice.Last(), m.AwayPrice.Last(), m.DrawPrice.Last()); err != nil {
			log.Printf("Failed to insert match price %s %s %s at %s error(%v)\n", m.League, m.Home, m.Away, m.StartsAt.String(), err)
		}
	}
}

func getMarket(m []*background.Market) []*background.Market {
	return market.SortByStart(
		market.Filter(m, func(item background.Market) bool {
			maxPricesLength := 2
			// before 1 hour
			a := item.StartsAt.Before(time.Now().Add(time.Hour * 1))
			// last price lower than first
			b := item.HomePrice.First() > item.HomePrice.Last()
			// price in range
			c := item.HomePrice.First() >= 2
			// home price goes down
			d := func(item background.Market) bool {
				hLen := len(item.HomePrice)
				if hLen > maxPricesLength {
					latest := item.HomePrice.First()
					for i := 1; i < hLen; i++ {
						if latest > item.HomePrice[i] {
							latest = item.HomePrice[i]
						} else {
							return false
						}
					}
					return true
				}
				return false
			}(item)
			// more than three price in list
			e := len(item.HomePrice) > maxPricesLength
			// last prices in order home draw away
			f := func(item background.Market) bool {
				hl := item.HomePrice.Last()
				dl := item.DrawPrice.Last()
				al := item.AwayPrice.Last()
				return (hl < dl) && (dl < al)
			}(item)

			// first price in order home draw away
			g := func(item background.Market) bool {
				hf := item.HomePrice.First()
				df := item.DrawPrice.First()
				af := item.AwayPrice.First()
				return (hf < df) && (df < af)
			}(item)
			return a && b && c && d && e && f && g
		}))
}

func (s *service) GetMarket() []*background.Market {
	return getMarket(s.scanner.GetMarket())
}

func (s *service) GetLive() (pinnacle.Live, error) {
	return s.scanner.GetLive()
}

func (s *service) ConnectToDB(dataSourceName string) error {
	return repository.Connect(dataSourceName)
}

func (s *service) DisconnectFromDB() error {
	log.Println("Closing db connection")
	return repository.Disconnect()
}
